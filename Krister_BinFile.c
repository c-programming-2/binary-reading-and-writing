#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define MAX_KUU 12
#define BUFFLEN 1024

#define STATISTIKA      "stats.bin"

//sisend failia andmed
typedef struct 
{

    uint8_t paev,kuu;                 //1-baidine taisarv
    uint16_t aasta;                    //2-baidine taisarv
    float vooluhulk;                   //4-baidine reaalarv

}sisendFil_t;

//valjund Fail 
typedef struct
{
    uint16_t tyyp;                  //2-bidine taisarv
    uint16_t aasta;                //2-baidine tasiarv
    float MinMaxKuu[12];
}valjund_t;

//avan faili
FILE *ava(const char *file, const char* tyyp)
{
    FILE *ptr = fopen(file,tyyp);
    if(ptr == NULL)
    {
        printf("Ei saanud faili nimega '%s' avada",file);
        exit(0);
    }
    
    return ptr;
}

//loen ridu
int loeread(FILE *f)
{
    fseek(f, 0, SEEK_END);                                          //liigume faili lõppu
	int count = ftell(f) / sizeof(sisendFil_t);            //kui palju praeguses asukohas baite faili algusest on / kirje suurus
    
    return count;
}
void korrigeeri(char *s)
{
	int p = strlen(s);
	while(p > 0 && (s[p-1] == '\n' || s[p-1] == '\r'))
		s[--p] = '\0'; // vähendame p väärtust ja kirjutame teksti vastavale kohale tekstilõpetamise märgi
}

//leiame valjund faili suuruse
int loeAastad(sisendFil_t *sisend,int count)
{
    int n = 1;
    int aasta = sisend[0].aasta;
   
    for(int i = 0 ; i < count;i++)
    {
        if(aasta != sisend[i].aasta)
        {
            aasta = sisend[i].aasta;
            
            n++;
        }
    }

    return n*2 ;
}

//Sorting data highest to lowest 
int compfnc(const void *a, const void *b)
{
	sisendFil_t *i1 = (sisendFil_t *)a;
	sisendFil_t *i2 = (sisendFil_t *)b;
	
		return i1->aasta -  i2->aasta;

}

//koostame statistika
void koostameStatistika(sisendFil_t*  sisend,int sisLen,valjund_t* valjund,int valLen)
{
    //taidame valjund massiivi aastatega ka dubleerime et saada to aasta max ja min 
    int aasta = 0;
    int n = 0;
    int currentKuu = 0;
   float maxKuu = sisend[0].vooluhulk;
   float minKuu = sisend[0].vooluhulk;
    
    
    for(int i = 0; i < sisLen;i++)
    {
        if(aasta != sisend[i].aasta)
        {
           
            
            aasta = sisend[i].aasta;
            valjund[n].tyyp = 1;//min vooluhulk
            valjund[n].aasta = sisend[i].aasta;
            n++;
            valjund[n].aasta = sisend[i].aasta;
            valjund[n].tyyp = 9;//max vooluhulk
            n++;
            
        }
    }

   for(int i = 0; i < valLen;i++)
    {
        for(int j = 0; j < MAX_KUU;j++)
        {
            valjund[i].MinMaxKuu[j] = 0.0;
        }
    }
  
   
 
   
   
   n = 0;
   aasta = sisend[0].aasta;
   int i = 0;
   while(i < sisLen)
   {
       if(aasta == sisend[i].aasta)
       {


            currentKuu = sisend[i].kuu - 1;
            while(currentKuu == sisend[i].kuu -1 && aasta== sisend[i].aasta)
            {
                //printf("%d\n",currentKuu);
                if(maxKuu < sisend[i].vooluhulk)
                {
   
                    maxKuu = sisend[i].vooluhulk;
                }
                if(minKuu > sisend[i].vooluhulk)
                {

                    minKuu = sisend[i].vooluhulk;
                }
               
               i++;
            }
            if(currentKuu != sisend[i].kuu )
            {       
 
                if(minKuu != 9999)
                {
                    valjund[n].MinMaxKuu[currentKuu] = minKuu;
                }
                
               
               minKuu = 9999;
               
                n++;
                
                if(maxKuu != -1)
                {
                    valjund[n].MinMaxKuu[currentKuu] = maxKuu;
                }
                
              
                maxKuu = -1;
               
                n--;

            }
            if(aasta != sisend[i].aasta)
            {
                aasta = sisend[i].aasta;
                n+= 2;

            }
       }
   }

   /*for(int i = 0; i < valLen;i++)
   {
       printf("%d %d ",valjund[i].tyyp,valjund[i].aasta);
       for(int j = 0 ; j< MAX_KUU;j++)
       {
           printf("%0.2f ",valjund[i].MinMaxKuu[j]);
       }
       printf("\n");
   }*/
  
   
   
   
}
void salvesta(FILE *f,int n,valjund_t* valjund)
{
    fwrite(valjund,sizeof(valjund_t),n,f);
}
int main(void)
{
    char srcFile[BUFFLEN];
    char dstFile[BUFFLEN];
    
    printf("Sisesta sisend fail: ");
    fgets(srcFile,BUFFLEN,stdin);
    korrigeeri(srcFile);
    printf("Sisesnta valjund fail: ");
    fgets(dstFile,BUFFLEN,stdin);
    korrigeeri(dstFile);
    if(strcmp(srcFile, dstFile) == 0)// samad failinimed
    { 
		printf("Sama failinimi, mis sisendil, kas kirjutame üle? (j/e): ");
		char buf[BUFFLEN];
		scanf("%s", buf);
		if(buf[0] == 'e' || buf[0] == 'E'){
			return 0;
		}
	}
    //avan faili
    FILE *f = ava(srcFile,"rb");
    FILE  *out = ava(dstFile,"wb");
    //loen ridu
    int count = loeread(f);
    
    sisendFil_t sisend[count];
    
    //faili algusess tagasi
    fseek(f,0,SEEK_SET);
    //loen andmeid ja leian tegeliku suuruse
    int n = fread(sisend,sizeof(sisendFil_t),count,f);
    fclose(f); // sulgeme faili, ei ole enam seda vaja
    printf("n/c\t%d|%d\n",count,n);//kontrollin
    //leiame valjund faili suuruse
    int valLen = loeAastad(sisend,n);
    valjund_t valjund[valLen];

    //sorteerin massiivi aastate jargi
    qsort(sisend,n,sizeof(sisendFil_t),compfnc);


    koostameStatistika(sisend,n,valjund,valLen);
    salvesta(out,valLen,valjund);
    fclose(out);
    
    return 0;
}
